package com.uhcl.mywatchface;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;

public class QuizDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="MyAwesomeQuiz.db";
    private static final int DATABASE_VERSION=2;
    private SQLiteDatabase db;
    private Question complicationQuestion;
    private List<Question> questionList;

    public QuizDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.db=sqLiteDatabase;
        final String SQL_CREATE_QUESTIONS_TABLE="create table "+ QuestionTable.TABLE_NAME
                +" ( " + QuestionTable._ID +" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuestionTable.COLUMN_QUESTION + " TEXT, " +
                QuestionTable.COLUMN_OPTION1 + " TEXT, " +
                QuestionTable.COLUMN_OPTION2 + " TEXT, " +
                QuestionTable.COLUMN_OPTION3 + " TEXT, " +
                QuestionTable.COLUMN_OPTION4 + " TEXT, " +
                QuestionTable.COLUMN_ANSWER_NR + " INTEGER " + ")";
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        fillQuestionTable();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        fillQuestionTable();
    }

    public void fillQuestionTable(){
        /*Question question1=new Question("Having a harmful effect; injurious","Deleterious","Bowdlerize","Nonsectarian","Nihilism",1);
        addQuestion(question1);
        Question question3=new Question("Falling off or shed at a specific season or stage of growth","Deciduous","Hegemony","Reparation","Impeach",1);
        addQuestion(question3);
        Question question4=new Question("The iron-containing respiratory pigment in red blood cells of vertebrates","Obsequious","Hemoglobin","Respiration","Laissez",2);
        addQuestion(question4);
        Question question5=new Question("To bring under control; conquer","Laissez faire","Reciprocal","Lexicon","Subjugate",4);
        addQuestion(question5);
        Question question6=new Question("To show servile deference","Enfranchise","Kowtow","Lugubrious","Antebellum",2);
        addQuestion(question6);
        Question question7=new Question("Division into ordered groups or categories","Abstemious","Inculcate","Taxonomy","Vortex",3);
        addQuestion(question7);
        Question question9=new Question("The lowest point","Usurp","Nadir","Infrastructure","Polymer",2);
        addQuestion(question9);
        Question question10=new Question("Having one&rsquo;s true identity concealed","Irony","Suffragist","Incognito","Homogeneous",3);
        addQuestion(question10);
        Question question11=new Question("The side of a right triangle opposite the right angle","Notarize","Detritus","Supercilious","Hypotenuse",4);
        addQuestion(question11);
        Question question14=new Question("A diligent, dependable worker","Quotidian","Loquacious","Yeoman","Parabola",3);
        addQuestion(question14);
        Question question15=new Question("A system of names used in an art or science","Metamorphosis","Acumen","Nomenclature","Quasar",3);
        addQuestion(question15);
        Question question17=new Question("To separate or get rid of (an undesirable part); eliminate","Gamete","Winnow","Wrought","Diffident",2);
        addQuestion(question17);
        Question question18=new Question("Not interesting; dull","Parameter","Hubris","Jejune","Facetious",3);
        addQuestion(question18);
        Question question19=new Question("Logical incongruity","Abjure","Contradiction","Belie","Moiety",2);
        addQuestion(question19);
        Question question20=new Question("Vanishing or likely to vanish like vapor","Recapitulate","Lucid","Evanescent","Omnipotent",3);
        addQuestion(question20);*/

        Question question1=new Question("remote","automatic","distant","savage","mean",2);
        addQuestion(question1);
        Question question2=new Question("detest","argue","hate","discover","reveal",2);
        addQuestion(question2);
        Question question3=new Question(" gracious","pretty","clever","pleasant","present",3);
        addQuestion(question3);
        Question question4=new Question("predict","foretell","decide","prevent","discover",1);
        addQuestion(question4);
        Question question5=new Question("kin","exult","twist","friend","relative",4);
        addQuestion(question5);
        Question question6=new Question(" pensive","oppressed","caged","thoughtful","happy",3);
        addQuestion(question6);
        Question question7=new Question(" banish","exile","hate","fade","clean",1);
        addQuestion(question7);
        Question question8=new Question(" fraud","malcontent","argument","imposter","clown",3);
        addQuestion(question8);
        Question question9=new Question(" saccharine","leave","sweet","arid","quit",2);
        addQuestion(question9);
        Question question10=new Question("drag","sleepy","crush","proud","pull",4);
        addQuestion(question10);
        Question question11=new Question("jovial","incredulous","merry","revolting","dizzy",2);
        addQuestion(question11);
        Question question12=new Question(" indifferent","neutral","unkind","precious","mean",1);
        addQuestion(question12);
        Question question13=new Question(" simulate","excite","imitate","trick","apelike",2);
        addQuestion(question13);
        Question question14=new Question(" charisma","ghost","force","charm","courage",3);
        addQuestion(question14);
        Question question15=new Question(" apportion","divide","decide","cut","squabble",1);
        addQuestion(question15);
        /*Question question16=new Question("generic","general","cheap","fresh","elderly",1);
        addQuestion(question16);
        Question question17=new Question(" qualm","distress","impunity","persevere","scruple",4);
        addQuestion(question17);
        Question question18=new Question(" wary","calm","curved","confused","cautious",4);
        addQuestion(question18);
        Question question19=new Question("distort","wrong","evil","deform","harm",3);
        addQuestion(question19);
        Question question20=new Question("sumptuous","delirious","gorgeous","perilous","luxurious",4);
        addQuestion(question20);

        Question question21=new Question("reel","whirl","ﬁsh","hit","mistake",1);
        addQuestion(question21);
        Question question22=new Question(" inscrutable","difﬁcult","mysterious","inﬂexible","wary",2);
        addQuestion(question22);
        Question question23=new Question(" appall","delirious","covered","dismay","confuse",3);
        addQuestion(question23);
        Question question24=new Question(" upright","honorable","horizontal","humble","supine",1);
        addQuestion(question24);
        Question question25=new Question("reverie","palimpsest","phantom","daydream","curio",3);
        addQuestion(question25);
        Question question26=new Question("loot","destruction","waste","spoils","cavort",3);
        addQuestion(question26);
        Question question27=new Question(" loquacious","talkative","thirsty","beautiful","complicated",1);
        addQuestion(question27);
        Question question28=new Question("chimera","chimney","protest","illusion","panache",3);
        addQuestion(question28);
        Question question29=new Question("temerity","audacity","fearfulness","shyness","stupidity",1);
        addQuestion(question29);
        Question question30=new Question("educe","demand","elicit","ideal","unlawful",2);
        addQuestion(question30);
        Question question31=new Question("nabob","bigwig","doubter","frolic","converse",1);
        addQuestion(question31);
        Question question32=new Question("pall","light","satiate","carry","horror",2);
        addQuestion(question32);
        Question question33=new Question(" sacrosanct","prayer","sanctuary","pious","sacred",4);
        addQuestion(question33);
        Question question34=new Question("louche","gauche","ﬁne","brilliant","indecent",4);
        addQuestion(question34);
        Question question35=new Question(" stentorian","violent","misbegotten","loud","stealthy",3);
        addQuestion(question35);*/

    }

    private void addQuestion(Question question){
        ContentValues contentValues=new ContentValues();
        contentValues.put(QuestionTable.COLUMN_QUESTION,question.getQuestion());
        contentValues.put(QuestionTable.COLUMN_OPTION1,question.getOption1());
        contentValues.put(QuestionTable.COLUMN_OPTION2,question.getOption2());
        contentValues.put(QuestionTable.COLUMN_OPTION3,question.getOption3());
        contentValues.put(QuestionTable.COLUMN_OPTION4,question.getOption4());
        contentValues.put(QuestionTable.COLUMN_ANSWER_NR,question.getAnswerNr());
        db.insert(QuestionTable.TABLE_NAME,null,contentValues);
    }

    public List<Question> getAllQuestions(){
        List<Question> questionList=new ArrayList<>();
        db= getReadableDatabase();
        Cursor cursor=db.rawQuery("select * from "+ QuestionTable.TABLE_NAME,null);
        if(cursor.moveToFirst()){
            do{
                Question question=new Question();
                question.setQuestion(cursor.getString(cursor.getColumnIndex(QuestionTable.COLUMN_QUESTION)));
                question.setOption1(cursor.getString(cursor.getColumnIndex(QuestionTable.COLUMN_OPTION1)));
                question.setOption2(cursor.getString(cursor.getColumnIndex(QuestionTable.COLUMN_OPTION2)));
                question.setOption3(cursor.getString(cursor.getColumnIndex(QuestionTable.COLUMN_OPTION3)));
                question.setOption4(cursor.getString(cursor.getColumnIndex(QuestionTable.COLUMN_OPTION4)));
                question.setAnswerNr(cursor.getInt(cursor.getColumnIndex(QuestionTable.COLUMN_ANSWER_NR)));
                questionList.add(question);

            }while (cursor.moveToNext());

        }

        cursor.close();
        return questionList;

    }

    public void SetComplicationQuestion()
    {
        questionList= getAllQuestions();
        Collections.shuffle(questionList);
        complicationQuestion = questionList.get(1);
    }

    public Question GetComplicationQuestion()
    {
        if(complicationQuestion == null)
            SetComplicationQuestion();
        return complicationQuestion;
    }

    public Question GetNewQuestion()
    {
        questionList= getAllQuestions();
        Collections.shuffle(questionList);
        return questionList.get(1);
    }
}
