package com.uhcl.mywatchface;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class MyQuizActivity extends WearableActivity {

    private TextView question;
    private RadioGroup radioGroup;
    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private RadioButton rb4;
    private Question currentQuestion;
    private Question newQuestion;
    private List<Question> questionList;
    private ProgressBar bar;
    RadioButton selectedRadio;
    RadioButton correctRadio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_my_quiz);

        question = (TextView) findViewById(R.id.question);
        question.setSelected(true);
        radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
        rb1=(RadioButton)findViewById(R.id.radio_button1);
        rb2=(RadioButton)findViewById(R.id.radio_button2);
        rb3=(RadioButton)findViewById(R.id.radio_button3);
        rb4=(RadioButton)findViewById(R.id.radio_button4);
        bar = (ProgressBar)findViewById(R.id.progressBar);

        Intent intent1 = getIntent();
        String name = intent1.getStringExtra("EXTRA_PROVIDER_INFO");
        Log.v("Check",name);
        System.out.println(name + "abc");
        if(radioGroup.getCheckedRadioButtonId() == -1) {
            newQuestion = new QuizDBHelper(getApplicationContext()).GetNewQuestion();
            showNewQuestion();
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
                selectedRadio = radioGroup.findViewById(selectedRadioButtonId);
                int selectedIndex = radioGroup.indexOfChild(selectedRadio);
                if(newQuestion.getAnswerNr()-1 == selectedIndex) {
                    Log.v("selection", "correct");
                    selectedRadio.setTextColor(Color.GREEN);
                    bar.setProgress(bar.getProgress() + 10);
                }
                else {
                    Log.v("selection", "wrong");
                    selectedRadio.setTextColor(Color.RED);
                    correctRadio = ((RadioButton)radioGroup.getChildAt(newQuestion.getAnswerNr()-1));
                    correctRadio.setTextColor(Color.GREEN);

                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        selectedRadio.setTextColor(Color.WHITE);
                        selectedRadio.setChecked(false);
                        newQuestion= new QuizDBHelper(getApplicationContext()).GetNewQuestion();
                        showNewQuestion();
                    }
                }, 3000);

            }
        });

        /*currentQuestion= new QuizDBHelper(this).GetComplicationQuestion();
        showNextQuestion();*/

       // Enables Always-on
       setAmbientEnabled();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Intent intent1 = getIntent();
        String name = intent1.getStringExtra("EXTRA_PROVIDER_INFO");
        Log.v("Check",name);
    }

    private static final String EXTRA_PROVIDER_COMPONENT =
            "com.example.android.wearable.watchface.provider.action.PROVIDER_COMPONENT";
    private static final String EXTRA_COMPLICATION_ID =
            "com.example.android.wearable.watchface.provider.action.COMPLICATION_ID";

    static PendingIntent getToggleIntent(
            Context context, ComponentName provider, int complicationId) {
        Intent intent = new Intent(context, MyQuizActivity.class);
        intent.putExtra(EXTRA_PROVIDER_COMPONENT, provider);
        intent.putExtra(EXTRA_COMPLICATION_ID, complicationId);

        // Pass complicationId as the requestCode to ensure that different complications get
        // different intents.
        return PendingIntent.getBroadcast(
                context, complicationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void showNextQuestion(){
        question.setText(currentQuestion.getQuestion());
        rb1.setTextColor(Color.WHITE);
        rb1.setText(currentQuestion.getOption1());
        rb2.setTextColor(Color.WHITE);
        rb2.setText(currentQuestion.getOption2());
        rb3.setTextColor(Color.WHITE);
        rb3.setText(currentQuestion.getOption3());
        rb4.setTextColor(Color.WHITE);
        rb4.setText(currentQuestion.getOption4());
    }

    private void showNewQuestion(){
        question.setText(newQuestion.getQuestion());
        rb1.setTextColor(Color.WHITE);
        rb1.setText(newQuestion.getOption1());
        rb2.setTextColor(Color.WHITE);
        rb2.setText(newQuestion.getOption2());
        rb3.setTextColor(Color.WHITE);
        rb3.setText(newQuestion.getOption3());
        rb4.setTextColor(Color.WHITE);
        rb4.setText(newQuestion.getOption4());
    }

}
